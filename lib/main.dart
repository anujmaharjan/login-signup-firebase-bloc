import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_signup_bloc_app/blocs/authentication_bloc/authentication_bloc.dart';
import 'package:login_signup_bloc_app/blocs/authentication_bloc/authentication_event.dart';
import 'package:login_signup_bloc_app/pages/homepage.dart';
import 'package:login_signup_bloc_app/pages/loginpage.dart';
import 'package:login_signup_bloc_app/pages/reset_password_page.dart';
import 'package:login_signup_bloc_app/pages/signuppage.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'blocs/authentication_bloc/authentication_state.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final UserRepository userRepository = UserRepository();
  // User user;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context) => AuthenticationBloc(userRepository: userRepository)..add(AppInitialisedEvent()),
        child: FirebaseApp(userRepository: userRepository)
      ),
    );
  }
}

class FirebaseApp extends StatelessWidget {
  final UserRepository userRepository;

  FirebaseApp({@required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, state) {
        if (state is AuthenticationInitialState) {
          return LoginPageParent(userRepository: userRepository);
        //   return UninitialisedPage();
        }
        else if (state is AuthenticatedState) {
          return HomePageParent(user: state.user, userRepository: userRepository);
        }
        else if (state is UnauthenticatedState) {
          return LoginPageParent(userRepository: userRepository);
        }
      },
    );
  }
}

