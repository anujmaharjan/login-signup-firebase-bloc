import 'package:bloc/bloc.dart';
import 'package:login_signup_bloc_app/blocs/homepage/homepage_event.dart';
import 'package:login_signup_bloc_app/blocs/homepage/homepage_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState>{
  UserRepository userRepository;
  HomePageBloc({this.userRepository}) : super(LogoutInitialState());

  @override
  Stream<HomePageState> mapEventToState(HomePageEvent event) async*{
    if (event is LogoutButtonPressedEvent){
      yield LogoutLoadingState();
      await userRepository.logOut();
      yield LogoutSuccessState();
    }
    // // TODO: implement mapEventToState
    // throw UnimplementedError();
  }
  
}