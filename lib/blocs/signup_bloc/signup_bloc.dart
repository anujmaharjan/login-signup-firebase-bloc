import 'package:bloc/bloc.dart';
import 'package:login_signup_bloc_app/blocs/signup_bloc/signup_event.dart';
import 'package:login_signup_bloc_app/blocs/signup_bloc/signup_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';

class UserSignUpBloc extends Bloc<UserSignUpEvent, UserSignUpState>{
  UserRepository userRepository;

  UserSignUpBloc({this.userRepository}) : super(UserSignUpInitialState());

  @override
  Stream<UserSignUpState> mapEventToState(UserSignUpEvent event) async*{
    if (event is SignUpButtonPressedEvent){
      try{
        yield UserSignUpInitialState();
        var user = await userRepository.createUser(event.email, event.password);
        print("SignedUp----user >>>>>>>>> $user");
        yield UserSignUpSuccessState(user: user);
        userRepository.logOut();
      }
      catch (e){
        yield UserSignUpFailedState(message: e.toString());
  }
  }
    // // TODO: implement mapEventToState
    // throw UnimplementedError();
  }

}