import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

abstract class UserSignUpState extends Equatable{}

class UserSignUpInitialState extends UserSignUpState{
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class UserSignUpLoadingState extends UserSignUpState{
  @override
  // TODO: implement props
  List<Object> get props => null;

}

class UserSignUpSuccessState extends UserSignUpState{
  final User user;
  UserSignUpSuccessState({
    @required this.user
  });
  @override
  // TODO: implement props
  List<Object> get props => null;

}

class UserSignUpFailedState extends UserSignUpState{
  String message;
  UserSignUpFailedState({
    @required this.message
  });
  @override
  // TODO: implement props
  List<Object> get props => null;

}