import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class UserSignUpEvent extends Equatable{}

class SignUpButtonPressedEvent extends UserSignUpEvent{
  final String email, password;
  SignUpButtonPressedEvent({
    @required this.email, @required this.password
});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}