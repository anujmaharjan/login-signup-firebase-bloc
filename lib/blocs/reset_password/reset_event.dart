import 'package:equatable/equatable.dart';

abstract class ResetPasswordEvent extends Equatable{}

class ResetPasswordButtonPressedEvent extends ResetPasswordEvent{
  final String email;
  ResetPasswordButtonPressedEvent ({this.email});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}