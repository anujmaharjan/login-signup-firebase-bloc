import 'package:equatable/equatable.dart';

abstract class ResetPasswordState extends Equatable{}

class ResetPasswordInitialState extends ResetPasswordState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class ResetPasswordLoadingState extends ResetPasswordState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class ResetPasswordSentState extends ResetPasswordState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

class ResetPasswordFailedState extends ResetPasswordState{
  final String message;
  ResetPasswordFailedState ({this.message});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}