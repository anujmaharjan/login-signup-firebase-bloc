import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:login_signup_bloc_app/blocs/reset_password/reset_event.dart';
import 'package:login_signup_bloc_app/blocs/reset_password/reset_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';

class ResetPasswordBloc extends Bloc<ResetPasswordEvent, ResetPasswordState>{
  UserRepository userRepository;

  ResetPasswordBloc({this.userRepository}) : super(ResetPasswordInitialState());

  @override
  Stream<ResetPasswordState> mapEventToState(ResetPasswordEvent event) async*{
    // TODO: implement mapEventToState

    // yield ResetPasswordInitialState();
    if (event is ResetPasswordButtonPressedEvent) {
      yield ResetPasswordLoadingState();
      try{
      var reset = await userRepository.resetPassword(event.email);
      yield ResetPasswordSentState();
    }
    catch(e){
      yield ResetPasswordFailedState(message: e.toString());
    }

    }
    throw UnimplementedError();
  }

}