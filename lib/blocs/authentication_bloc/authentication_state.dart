import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

abstract class AuthenticationState extends Equatable{}

class AuthenticationInitialState extends AuthenticationState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}
class AuthenticatedState extends AuthenticationState{
  final User user;
  AuthenticatedState({@required this.user});

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}
class UnauthenticatedState extends AuthenticationState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}