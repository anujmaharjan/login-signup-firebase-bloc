import 'package:bloc/bloc.dart';
import 'package:login_signup_bloc_app/blocs/authentication_bloc/authentication_event.dart';
import 'package:login_signup_bloc_app/blocs/authentication_bloc/authentication_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>{
  UserRepository userRepository;
  AuthenticationBloc({this.userRepository}) : super(AuthenticationInitialState());

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
    if (event is AppInitialisedEvent) {
      try {
        var isLoggedIn = await userRepository.isLoggedIn();
        if (isLoggedIn)
        {
          var user = await userRepository.getCurrentUser();
          yield AuthenticatedState(user: user);
        }
        else {
          yield UnauthenticatedState();
        }
      }
      catch (e) {
        yield UnauthenticatedState();
      }
      // // TODO: implement mapEventToState
      // throw UnimplementedError();
    }
  }
}