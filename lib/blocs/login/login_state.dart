import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

abstract class UserLoginState extends Equatable{}

class UserLoginInitialState extends UserLoginState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class UserLoginLoadingState extends UserLoginState{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

class UserLoginSuccessState extends UserLoginState{
  final User user;
  UserLoginSuccessState({
    @required this.user
  });
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

class UserLoginFailedState extends UserLoginState{
  final String message;
  UserLoginFailedState({
    @required this.message
  });
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

//
// class GoogleSignInSuccessState extends UserLoginState{
//   final User user;
//   GoogleSignInSuccessState({
//     @required this.user
//   });
//   @override
//   // TODO: implement props
//   List<Object> get props => throw UnimplementedError();
//
// }

// class GoogleSignInLoadingState extends UserLoginState{
//   @override
//   // TODO: implement props
//   List<Object> get props => throw UnimplementedError();
// }
//
// class GoogleSignInFailedState extends UserLoginState{
//   final String message;
//   GoogleSignInFailedState ({this.message});
//
//   @override
//   // TODO: implement props
//   List<Object> get props => throw UnimplementedError();
//
// }