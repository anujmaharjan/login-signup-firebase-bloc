import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class UserLoginEvent extends Equatable{}

class LoginButtonPressedEvent extends UserLoginEvent{
  final String email, password;
  LoginButtonPressedEvent({
    @required this.email, @required this.password
  });

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

class GoogleButtonPressedEvent extends UserLoginEvent{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class FacebookButtonPressedEvent extends UserLoginEvent{
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}