import 'package:bloc/bloc.dart';
import 'package:login_signup_bloc_app/blocs/login/login_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'login_event.dart';

class UserLoginBloc extends Bloc<UserLoginEvent, UserLoginState> {
  UserRepository userRepository;

  UserLoginBloc({this.userRepository}) : super(UserLoginInitialState());

  @override
  Stream<UserLoginState> mapEventToState(UserLoginEvent event) async*{

    if (event is LoginButtonPressedEvent){
      yield UserLoginLoadingState();
      try{
      var user = await userRepository.loginUser(event.email, event.password);
      print("Custom----user >>>>>>>>> $user");
      yield UserLoginSuccessState(user: user);
    }
      catch (e){
        yield UserLoginFailedState(message: e.toString());
      }
    }

    if (event is GoogleButtonPressedEvent){
      yield UserLoginLoadingState();
      try{
        var user = await userRepository.signInWithGoogle();
        print("Google----user >>>>>>>>> $user");
        // yield UserLoginSuccessState(user: user);
        yield UserLoginSuccessState(user: user);
      }
      catch(e){
        yield UserLoginFailedState(message: e.toString());
      }
    }

    if (event is FacebookButtonPressedEvent){
      yield UserLoginLoadingState();
      try{
        var user = await userRepository.loginWithFacebook();
        print("Facebook----user >>>>>>>>> $user");
        // yield UserLoginSuccessState(user: user);
        if (user!=null) {
          yield UserLoginSuccessState(user: user);
        }
        else{
          yield UserLoginInitialState();
        }
      }
      catch(e){
        yield UserLoginFailedState(message: e.toString());
      }
    }
    // // TODO: implement mapEventToState
    // throw UnimplementedError();
  }
}