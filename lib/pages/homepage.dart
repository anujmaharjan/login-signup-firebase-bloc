import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_signup_bloc_app/blocs/homepage/homepage_bloc.dart';
import 'package:login_signup_bloc_app/blocs/homepage/homepage_event.dart';
import 'package:login_signup_bloc_app/blocs/homepage/homepage_state.dart';
import 'package:login_signup_bloc_app/pages/signuppage.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'loginpage.dart';

class HomePageParent extends StatelessWidget {
  final User user;
  final UserRepository userRepository;

  HomePageParent({@required this.user, @required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomePageBloc(userRepository: userRepository),
      child: HomePage(user: user, userRepository: userRepository,),
    );
  }
}

class HomePage extends StatefulWidget {
  User user;
  UserRepository userRepository;

  HomePage({@required this.user, @required this.userRepository});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomePageBloc homePageBloc;

  void initState() {
    super.initState();
    homePageBloc = HomePageBloc(userRepository: widget.userRepository);
  }


  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child:Text("Logging out" )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    homePageBloc = BlocProvider.of<HomePageBloc>(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 260,
              width: double.infinity,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    bottom: 0,
                    left: -100,
                    top: -150,
                    child: Container(
                      width: 400,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [Colors.tealAccent[700], Colors.tealAccent[400]]
                          )
                      ),
                    ),
                  ),
                  Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                          colors: [Colors.tealAccent[400], Colors.tealAccent[100]]
                      )
                  ),
                  ),
                  Positioned(
                    top: 120,
                    right: 150,
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                              colors: [Colors.tealAccent[400], Colors.tealAccent[100]]
                          )
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 110, left: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text("Welcome Home", style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold))),
                        Container(
                          child: Text(widget.user.email, style: TextStyle(color: Colors.white, fontSize: 15)),
                        ),
                      ]
                    ),
                  ),
                ],
              ),
            ),

            Container(
                width: 150,
                child: Image.asset('assets/images/homepage-img.png', fit: BoxFit.contain),
              ),

           Container(
              child: Text("Overview", style: TextStyle(color: Colors.black, fontSize: 30))
            ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
            child: Container(
              alignment: Alignment.center,
              child: Text("A professional profile is an introductory section on your resume that highlights your relevant qualifications"
                        "First, your resume profile concisely features your skills and professional experience in a way that gets employers’ attention. ",
                  style: TextStyle(color: Colors.black54, fontSize: 20)),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 10),
            child: FlatButton(
              color: Colors.black, onPressed:(){
                homePageBloc.add(LogoutButtonPressedEvent());
              },
              child: Text(
                'Logout',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
          ),
          BlocConsumer<HomePageBloc, HomePageState>(
              listener: (context, state){
                if (state is LogoutSuccessState){
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      LoginPageParent(userRepository: widget.userRepository)), (Route<dynamic> route) => false);
                  // Navigator.pop(context);
                  // // Navigator.pop(context);
                  // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                  //   return LoginPageParent(userRepository: widget.userRepository);
                  // }));
                }
              },

              builder: (context, state){
                if (state is LogoutInitialState){
                  return Container();
                }
                else if (state is LogoutLoadingState) {
                  return Container();
                }
                else if (state is LogoutSuccessState) {
                  return Container();
                };
              }
          ),
          ],
        ),
      ),

    );
  }
}

 