import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login_signup_bloc_app/blocs/reset_password/reset_bloc.dart';
import 'package:login_signup_bloc_app/blocs/reset_password/reset_event.dart';
import 'package:login_signup_bloc_app/blocs/reset_password/reset_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'loginpage.dart';


class ResetPasswordPageParent extends StatelessWidget {
  final UserRepository userRepository;

  ResetPasswordPageParent({@required this.userRepository});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ResetPasswordBloc(userRepository: userRepository),
      child: ResetPasswordPage(userRepository: userRepository,),
    );
  }
}

class ResetPasswordPage extends StatefulWidget {
  final UserRepository userRepository;
  ResetPasswordPage({@required this.userRepository});

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  var _formKey = GlobalKey<FormState>();
  String _email = "";
  // TextEditingController _emailcontroller = TextEditingController();
  ResetPasswordBloc _resetPasswordBloc;

  TextEditingController _emailcontroller;

  @override
  void dispose() {
    _emailcontroller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void initState() {
    super.initState();
    _emailcontroller = TextEditingController();
    _emailcontroller.addListener(_updateEmail);
  }

  _updateEmail() {
    print("updated email");
    setState(() {
      _email = _emailcontroller.text;
    });
  }

  bool validateEmail({checkEmpty:false}) {
    return (_email.isEmpty && !checkEmpty) || RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
        .hasMatch(_emailcontroller.text);
  }

  void formSubmit() {
    _resetPasswordBloc.add(ResetPasswordButtonPressedEvent(email: _emailcontroller.text));
    if (!_formKey.currentState.validate()) {
      return;
    }
  }

  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child:Text("Loading" )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _resetPasswordBloc = BlocProvider.of<ResetPasswordBloc>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white24,
        elevation: 0,
        leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () {
                  Navigator.pop(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPageParent(userRepository: widget.userRepository),
                    ),
                  );
                },
              );
            }),
      ),
      body: Container(
        color: Colors.white24,
        padding: EdgeInsets.only(top: 60, left: 20, right: 20, bottom: 30),
        child: Form(
          key: _formKey,
          child: ListView(
              children: <Widget>[
                BlocConsumer<ResetPasswordBloc, ResetPasswordState>(
                    listener: (context, state) {
                      if (state is ResetPasswordSentState) {
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                            LoginPageParent(userRepository: widget.userRepository)), (Route<dynamic> route) => false);
                        Fluttertoast.showToast(
                          msg: "A link has been sent to your mail for password change",
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 5,
                          backgroundColor: Colors.green[800],
                          textColor: Colors.white,
                          fontSize: 18.0,
                        );
                      }

                      else if (state is ResetPasswordLoadingState){
                        showAlertDialog(context);
                      }

                      else if (state is ResetPasswordFailedState) {
                        print("error >> ${state.message}");
                        List<String> part = state.message.split(' ');
                        part.remove('Exception:');
                        String errorMessage = part.join(' ');

                        Fluttertoast.showToast(
                          msg: errorMessage,
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 5,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 18.0,
                        );
                        Navigator.pop(context);
                      }
                    },

                    builder: (context, state) {
                      return Container(
                          child: Column(
                            children: <Widget>[
                              Column(
                                  children: <Widget>[
                                    Container(
                                      height: 200,
                                      width: 200,
                                      child: Image.asset(
                                          'assets/images/resetpassword-img.png',
                                          fit: BoxFit.contain),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 10),
                                      child: Text("Reset Your Password",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30,
                                              color: Colors.black)),
                                    ),
                                    Container(
                                      child: Text(
                                          "You can set a new password to login",
                                          style: TextStyle(fontSize: 15,
                                              color: Colors.black87)),
                                    ),
                                  ]
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 20.0),
                                child: TextFormField(
                                  controller: _emailcontroller,
                                  style: TextStyle(color: Colors.black),
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  validator: (value) {
                                    if (_emailcontroller.text.length == 0 || !RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
                                            .hasMatch(_emailcontroller.text)) {
                                     return "Enter a valid email";
                                    };
                                    return null;
                                  },
                                  onChanged: (String email) {
                                    print(email);
                                    _email = email;
                                  },
                                  decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.email_outlined,
                                          color: Colors.black54),
                                      border: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(80.0)),
                                        borderSide: BorderSide.none,
                                        //borderSide: const BorderSide(),
                                      ),
                                      hintStyle: TextStyle(
                                          color: Colors.black54),
                                      filled: true,
                                      fillColor: Colors.white,
                                      hintText: 'Enter your email address'
                                  ),
                                ),
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 10, left: 80, right: 80),
                                  child: Container(
                                    height: 60,
                                    width: 200,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(30),
                                      child: RaisedButton(
                                        color: Colors.tealAccent[700],
                                        onPressed: validateEmail(checkEmpty: true) ? formSubmit: null,
                                        child: Text('Send Request', style: TextStyle(color: Colors.white, fontSize: 15),),
                                      ),
                                    ),
                                  )
                              ),
                            ],
                          )
                      );
                    } // builder: (context, state)
                ),
              ]
          ),
        ),
      ),

    );
  }

}


