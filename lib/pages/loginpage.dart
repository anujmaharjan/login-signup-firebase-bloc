import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:login_signup_bloc_app/blocs/login/login_bloc.dart';
import 'package:login_signup_bloc_app/blocs/login/login_event.dart';
import 'package:login_signup_bloc_app/blocs/login/login_state.dart';
import 'package:login_signup_bloc_app/pages/reset_password_page.dart';
import 'package:login_signup_bloc_app/pages/signuppage.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'homepage.dart';

class LoginPageParent extends StatelessWidget {
  final UserRepository userRepository;

  LoginPageParent({@required this.userRepository});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserLoginBloc(userRepository: userRepository),
      child: LoginPage(userRepository: userRepository),
    );
  }
}


class LoginPage extends StatefulWidget {
  final UserRepository userRepository;
  LoginPage({@required this.userRepository});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _formKey = GlobalKey<FormState>();
  bool _isChecked = false;
  bool _isHidden = true;
  String _password = "";
  String _email = "";

  UserLoginBloc _userLoginBloc;

  TextEditingController _emailcontroller;
  TextEditingController _passwordcontroller;

  @override
  void dispose() {
    _emailcontroller.dispose();
    _passwordcontroller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void initState() {
    super.initState();
    _emailcontroller = TextEditingController();
    _passwordcontroller = TextEditingController();

    _emailcontroller.addListener(_updateEmail);
    _passwordcontroller.addListener(_updatePassword);
  }

  _updateEmail() {
    print("updated email");
    setState(() {
      _email = _emailcontroller.text;
    });
  }

  _updatePassword() {
    print("updated password");
    setState(() {
      _password = _passwordcontroller.text;
    });
  }

  bool validateEmail({checkEmpty:false}) {
    return (_email.isEmpty && !checkEmpty) || RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
        .hasMatch(_emailcontroller.text);
  }

  bool validatePassword({checkEmpty:false}) {
    return  (_password.isEmpty && !checkEmpty) || _password.length != 0 ;
  }

  void formSubmit() {
    _userLoginBloc.add(LoginButtonPressedEvent(
        email: _emailcontroller.text, password: _passwordcontroller.text));
    if (!_formKey.currentState.validate()) {
      return;
    }
  }


  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child:Text("Loading" )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // _googleSignInBloc = BlocProvider.of<GoogleSignInBloc>(context);
    _userLoginBloc = BlocProvider.of<UserLoginBloc>(context);
    return Scaffold(
      body: Container(
              color: Colors.white24,
              padding: EdgeInsets.only(top: 60, left: 20, right: 20, bottom: 30),
              child: Form(
                key: _formKey,
                child: ListView(
                    children: <Widget>[
                      BlocConsumer<UserLoginBloc, UserLoginState>(
                          listener: (context, state){
                            if (state is UserLoginInitialState){
                              // Navigator.pop(context);
                              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                  LoginPageParent(userRepository: widget.userRepository)), (Route<dynamic> route) => false);

                            }

                            else if (state is UserLoginSuccessState){
                              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                  HomePageParent(user: state.user, userRepository: widget.userRepository)), (Route<dynamic> route) => false);

                              // // Navigator.pop(context);
                              // // Navigator.pop(context);
                              // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                              //   return HomePageParent(user: state.user, userRepository: widget.userRepository,);
                              // }));

                            }

                            else if (state is UserLoginLoadingState){
                              showAlertDialog(context);
                            }

                            else if (state is UserLoginFailedState) {
                              print("error >> ${state.message}");
                              List<String> part = state.message.split(' ');
                              part.remove('Exception:');
                              String errorMessage = part.join(' ');

                              Fluttertoast.showToast(
                                msg: errorMessage,
                                toastLength: Toast.LENGTH_SHORT,
                                timeInSecForIosWeb: 7,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 18.0,
                              );
                                Navigator.pop(context);
                              }


                          },

                          builder: (context, state) {
                            return Container(
                              child: Column(
                                  children: <Widget>[
                                    Container(
                                      height: 180,
                                      width: 160,
                                      child: Image.asset(
                                          'assets/images/login-img.png',
                                          fit: BoxFit.contain),
                                    ),

                                    Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Text("Welcome back!", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.black)),
                                          ),
                                          Container(
                                            child: Text(
                                              "Login your existing account",
                                              style: TextStyle(fontSize: 15, color: Colors.black54)),
                                          ),
                                        ]
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 20.0),
                                      child: TextFormField(
                                        controller: _emailcontroller,
                                        style: TextStyle(color: Colors.black),
                                        autovalidateMode: AutovalidateMode.onUserInteraction,
                                        validator: (value) {
                                          if (_emailcontroller.text.length == 0 || !RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
                                              .hasMatch(_emailcontroller.text)) {
                                            return "Enter a valid email";
                                          }
                                          return null;
                                        },
                                        onChanged: (String email) {
                                          print(email);
                                          _email = email;
                                        },
                                        decoration: InputDecoration(
                                            prefixIcon: Icon(
                                                Icons.email_outlined,
                                                color: Colors.black54),
                                            border: OutlineInputBorder(
                                              // width: 0.0 produces a thin "hairline" border
                                              borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                              borderSide: BorderSide.none,
                                              //borderSide: const BorderSide(),
                                            ),
                                            hintStyle: TextStyle(color: Colors.black54),
                                            filled: true,
                                            fillColor: Colors.white,
                                            hintText: 'Email address'
                                        ),
                                      ),
                                    ),

                                    Container(
                                      padding: EdgeInsets.only(top: 8.0),
                                      child: TextFormField(
                                        controller: _passwordcontroller,
                                        style: TextStyle(color: Colors.black),
                                        autovalidateMode: AutovalidateMode.onUserInteraction,
                                        obscureText: _isHidden,
                                        validator: (value) {
                                          if (_passwordcontroller.text.length == 0) {
                                            return "Please enter your password";
                                          }
                                        },
                                        onChanged: (String password) {
                                          print(password);
                                          _password = password;
                                        },
                                        decoration: InputDecoration(
                                            prefixIcon: Icon(Icons.lock_outline, color: Colors.black54),
                                            suffixIcon: InkWell(
                                              onTap: _togglePassword,
                                              child: Icon(_isHidden ? Icons.visibility_off : Icons.visibility, color: Colors.tealAccent[700]),
                                            ),
                                            border: OutlineInputBorder(
                                              // width: 0.0 produces a thin "hairline" border
                                              borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                              borderSide: BorderSide.none,
                                              //borderSide: const BorderSide(),
                                            ),
                                            hintStyle: TextStyle(color: Colors.black54),
                                            filled: true,
                                            fillColor: Colors.white,
                                            hintText: 'Password'
                                        ),
                                      ),
                                    ),


                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Row(
                                              children: <Widget>[
                                                Checkbox(
                                                  value: _isChecked,
                                                  activeColor: Colors.black,
                                                  onChanged: (bool value) {
                                                    print(value);
                                                    setState(() {
                                                      _isChecked = value;
                                                    });
                                                  },
                                                ),
                                                Text("Remember me", style: TextStyle(color: Colors.black, fontSize: 14.0)),
                                              ]
                                          ),

                                          FlatButton(
                                            onPressed: () =>
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        ResetPasswordPageParent(
                                                          userRepository: widget.userRepository,),
                                                  ),
                                                ),

                                            padding: EdgeInsets.only(top: 5),
                                            child: Text('Forgot Password?', style: TextStyle(color: Colors.black),),
                                          ),
                                        ]
                                    ),

                                    Container(
                                        padding: EdgeInsets.only(top: 10, left: 60, right: 60),
                                        child: Container(
                                          height: 60,
                                          width: 200,
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(30),
                                            child: RaisedButton(
                                              color: Colors.tealAccent[700],
                                              onPressed: validateEmail(checkEmpty: true) && validatePassword(checkEmpty: true) ? formSubmit: null,
                                              child: Text('LOGIN', style: TextStyle(color: Colors.white, fontSize: 15),),
                                            ),
                                          ),
                                        )
                                    ),

                                    Padding(
                                      padding: EdgeInsets.only(top: 30),
                                      child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Text("Or connect using", style: TextStyle(color: Colors.black38, fontSize: 18)),
                                            ),
                                            Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  Container(
                                                    padding: EdgeInsets.only(top: 15),
                                                    child: Container(
                                                      height: 50,
                                                      width: 150,
                                                      decoration: BoxDecoration(color: Colors.blue[800],
                                                          border: Border.all(color: Colors.blue[800]),
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: FlatButton(
                                                        onPressed: () {
                                                          _userLoginBloc.add(FacebookButtonPressedEvent());
                                                        },
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                          children: <Widget>[
                                                            Icon(FontAwesomeIcons.facebook, color: Colors.white, size: 20),
                                                            Text("Facebook", style: TextStyle(color: Colors.white, fontSize: 15)
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.only(top: 15),
                                                    child: Container(
                                                      height: 50,
                                                      width: 150,
                                                      decoration: BoxDecoration(
                                                          color: Colors.redAccent[700],
                                                          border: Border.all(color: Colors.redAccent[700]),
                                                          borderRadius: BorderRadius.circular(10)
                                                      ),
                                                      child: FlatButton(
                                                        onPressed: () {
                                                          _userLoginBloc.add(GoogleButtonPressedEvent());
                                                        },
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                          children: <Widget>[
                                                            Icon(FontAwesomeIcons.google, color: Colors.white, size: 20),
                                                            Text("Google", style: TextStyle(color: Colors.white, fontSize: 15)),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ]
                                            ),

                                            Padding(
                                              padding: EdgeInsets.only(top: 40),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text("Don't have an account?", style: TextStyle(color: Colors.black, fontSize: 15)),
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) => SignUpPageParent(userRepository: widget.userRepository),),
                                                      );
                                                    },
                                                      child: Text(" Sign up", style: TextStyle(color: Colors.blue[600], fontSize: 15))
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ]
                                      ),
                                    ),
                                  ]
                              ),
                            );
                          }
                    ),
                  ]
                ),
              ),
            ),
   );
  }


  void _togglePassword() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

}

