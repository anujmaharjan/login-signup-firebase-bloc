import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login_signup_bloc_app/blocs/signup_bloc/signup_bloc.dart';
import 'package:login_signup_bloc_app/blocs/signup_bloc/signup_event.dart';
import 'package:login_signup_bloc_app/blocs/signup_bloc/signup_state.dart';
import 'package:login_signup_bloc_app/repository/user_repository.dart';
import 'homepage.dart';
import 'loginpage.dart';

class SignUpPageParent extends StatelessWidget {
  final UserRepository userRepository;

  SignUpPageParent({@required this.userRepository});
  @override
  Widget build(BuildContext context) {
   return BlocProvider(
     create: (context) => UserSignUpBloc(userRepository: userRepository),
     child: SignUpPage(userRepository: userRepository),
   );
  }
}

class SignUpPage extends StatefulWidget {
  final UserRepository userRepository;
  SignUpPage({@required this.userRepository});


  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var _formKey = GlobalKey<FormState>();
  // bool Checked = false;
  bool _isHidden = true;
  String _password = "";
  String _email = "";
  String _username = "";
  String _phone = "";
  String _confpassword = "";

  UserSignUpBloc userSignUpBloc;

  TextEditingController _emailcontroller;
  TextEditingController _passwordcontroller;
  TextEditingController _confpasswordcontroller;
  TextEditingController _usernamecontroller;
  TextEditingController _phonecontroller;

  @override
  void dispose() {
    _emailcontroller.dispose();
    _passwordcontroller.dispose();
    _confpasswordcontroller.dispose();
    _usernamecontroller.dispose();
    _phonecontroller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void initState() {
    super.initState();
    _emailcontroller = TextEditingController();
    _passwordcontroller = TextEditingController();
    _confpasswordcontroller = TextEditingController();
    _phonecontroller = TextEditingController();
    _usernamecontroller = TextEditingController();

    _emailcontroller.addListener(_updateEmail);
    _passwordcontroller.addListener(_updatePassword);
    _confpasswordcontroller.addListener(_updateConfPassword);
    _phonecontroller.addListener(_updatePhone);
    _usernamecontroller.addListener(_updateUsername);
  }

  _updateEmail() {
    print("updated email");
    setState(() {
      _email = _emailcontroller.text;
    });
  }

  _updatePassword() {
    print("updated password");
    setState(() {
      _password = _passwordcontroller.text;
    });
  }

  _updateConfPassword() {
    print("updated confirm pw");
    setState(() {
      _confpassword = _confpasswordcontroller.text;
    });
  }

  _updatePhone() {
    print("updated phone num");
    setState(() {
      _phone = _phonecontroller.text;
    });
  }

  _updateUsername() {
    print("updated username");
    setState(() {
      _username = _usernamecontroller.text;
    });
  }


  bool validateEmail({checkEmpty:false}) {
    return (_email.isEmpty && !checkEmpty) || RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
        .hasMatch(_emailcontroller.text);
  }

  bool validatePassword({checkEmpty:false}) {
    return  (_password.isEmpty && !checkEmpty) || _password.length > 7 ;
  }

  bool validatePhone({checkEmpty:false}) {
    return  (_phone.isEmpty && !checkEmpty) || (RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$').hasMatch(_phone) && _phone.length == 10) ;
  }

  bool validateUsername({checkEmpty:false}) {
    return  (_username.isEmpty && !checkEmpty) || _username.length > 3;
  }

  bool validateConfPassword({checkEmpty:false}) {
    return  (_confpassword.isEmpty && !checkEmpty) || _confpassword == _password;
  }

  void formSubmit() {
    userSignUpBloc.add(SignUpButtonPressedEvent(
        email: _emailcontroller.text, password: _passwordcontroller.text));
    if (!_formKey.currentState.validate()) {
      return;
    }
  }


  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child:Text("Creating account")),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    userSignUpBloc = BlocProvider.of<UserSignUpBloc>(context);
      return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white24,
        elevation: 0,
        leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () {
                  Navigator.pop(context, MaterialPageRoute(
                      builder: (context) => LoginPageParent(userRepository: widget.userRepository),
                    ),
                  );
                },
              );
            }),
      ),

      body: Container(
        color: Colors.white24,
        padding: EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: ListView(
              children: <Widget>[
                BlocConsumer<UserSignUpBloc, UserSignUpState>(
                    listener: (context, state){
                      if (state is UserSignUpSuccessState){

                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                            LoginPageParent(userRepository: widget.userRepository)), (Route<dynamic> route) => false);
                        // Navigator.pop(context);
                        // Navigator.pop(context);
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                        //   return LoginPageParent(userRepository: widget.userRepository);
                        // }));

                        Fluttertoast.showToast(
                          msg: 'Successfully registered' ,
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 9,
                          backgroundColor: Colors.green[800],
                          textColor: Colors.white,
                          fontSize: 18.0,
                        );
                      }
                      else if (state is UserSignUpLoadingState){
                        showAlertDialog(context);
                      }

                      else if (state is UserSignUpFailedState) {
                        print("error >> ${state.message}");
                        List<String> part = state.message.split(' ');
                        part.remove('Exception:');
                        String errorMessage = part.join(' ');
                        Fluttertoast.showToast(
                          msg: errorMessage,
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 7,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 18.0,
                        );
                        // Navigator.pop(context);
                      }
                    },

                    builder: (context, state){
                      return Container(
                        child: Column(
                          children: <Widget>[
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(top: 14),
                                    child: Text("Let's get started!", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.black)),
                                  ),
                                  Container(
                                    child: Text("Create an account to login", style: TextStyle(fontSize: 15,color: Colors.black54)),
                                  ),
                                ]
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 30.0),
                              child: TextFormField(
                                controller: _usernamecontroller,
                                style: TextStyle(color: Colors.black),
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                validator: (String value) {
                                  if (value.length < 4) {
                                    return "Username must be at least 4 characters";
                                  }
                                },
                                onChanged: (String username) {
                                  print(username);
                                  _username = username;
                                },
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.perm_identity_sharp, color: Colors.black54),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                      borderSide: BorderSide.none,
                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(color: Colors.black54),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: 'Username'
                                ),
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 14.0),
                              child: TextFormField(
                                controller: _emailcontroller,
                                style: TextStyle(color: Colors.black),
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                validator: (value) {
                                  if (value.length == 0 || !RegExp("^(([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5}){1,25})+)*")
                                      .hasMatch(value)) {
                                    return "Enter a valid email";
                                  };
                                  return null;
                                },
                                onChanged: (String email) {
                                  print(email);
                                  _email = email;
                                },
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.email_outlined, color: Colors.black54),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                      borderSide: BorderSide.none,
                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(color: Colors.black54),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: 'Email'
                                ),
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 14.0),
                              child: TextFormField(
                                controller: _phonecontroller,
                                keyboardType: TextInputType.number,
                                style: TextStyle(color: Colors.black),
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                validator: (String value) {
                                  if (value.length != 10 || !RegExp(r"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$").hasMatch(value))
                                  {
                                    return "Invalid phone number";
                                  }
                                  return null;

                                },
                                onChanged: (String phone) {
                                  print(phone);
                                  _phone = phone;
                                },
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.phone_android, color: Colors.black54),
                                  border: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                    borderSide: BorderSide.none,
                                    //borderSide: const BorderSide(),
                                  ),
                                  hintStyle: TextStyle(color: Colors.black54),
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: 'Phone',

                                ),
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 14.0),
                              child: TextFormField(
                                controller: _passwordcontroller,
                                style: TextStyle(color: Colors.black),
                                obscureText: _isHidden,
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                validator: (String value) {
                                  if(value.length < 8)
                                  {
                                    return "Password must be at least 8 characters long";
                                  }
                                },
                                onChanged: (String password) {
                                  print(password);
                                  _password = password;
                                },
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.lock_outline, color: Colors.black54),
                                    suffixIcon: InkWell(
                                      onTap: _togglePassword,
                                      child: Icon(_isHidden ? Icons.visibility_off : Icons.visibility, color: Colors.tealAccent[700]),
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                      borderSide: BorderSide.none,
                                    ),
                                    hintStyle: TextStyle(color: Colors.black54),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: 'Password'
                                ),
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 18.0),
                              child: TextFormField(
                                controller: _confpasswordcontroller,
                                style: TextStyle(color: Colors.black),
                                obscureText: _isHidden,
                                autovalidateMode: AutovalidateMode.onUserInteraction,
                                validator: (String value) {
                                  if (_passwordcontroller.text != _confpasswordcontroller.text)
                                  {
                                    return "Passwords do not match";
                                  }
                                },
                                onChanged: (String confirmpassword) {
                                  print(confirmpassword);
                                  _confpassword = confirmpassword;
                                },
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.lock_outline, color: Colors.black54),
                                    suffixIcon: InkWell(
                                      onTap: _togglePassword,
                                      child: Icon(_isHidden ? Icons.visibility_off : Icons.visibility, color: Colors.tealAccent[700],),
                                    ),
                                    border: OutlineInputBorder(
                                      // width: 0.0 produces a thin "hairline" border
                                      borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                      borderSide: BorderSide.none,
                                      //borderSide: const BorderSide(),
                                    ),
                                    hintStyle: TextStyle(color: Colors.black54),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: 'Confirm Password'
                                ),
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(top: 20, left: 80, right: 80),
                                child: Container(
                                  height: 60,
                                  width: 200,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30),
                                    child: RaisedButton(
                                      color: Colors.tealAccent[700],
                                      onPressed: validateEmail(checkEmpty: true) && validatePassword(checkEmpty: true) && validateConfPassword(checkEmpty: true)
                                          && validatePhone(checkEmpty: true) && validateUsername(checkEmpty: true)  ? formSubmit: null,
                                      child: Text('SIGN UP', style: TextStyle(color: Colors.white, fontSize: 15),),
                                    ),
                                  ),
                                )
                            ),
                          ],
                        ),
                      );
                    }
                ),

              ]
          ),
        ),
      ),

    );
  }


  void _togglePassword() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}
