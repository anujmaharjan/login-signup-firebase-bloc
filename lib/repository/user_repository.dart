import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

class UserRepository {
  FirebaseAuth firebaseAuth;
  final googleSignIn = GoogleSignIn();


  UserRepository() {
    this.firebaseAuth = FirebaseAuth.instance;
  }

  Future<User> createUser(String email, String password) async {
    try {
      var result = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return result.user;
    }
    catch (e) {
      String authError="";
      print("exception >>>>>> ${e.code}");
      print("exception msg >>>>>> ${e.message}");
      switch(e.code){
        case 'email-already-in-use': authError = "This email is already in use";
        break;
        case 'network-request-failed': authError = "A network error has occurred";
        break;
        default:
          print("Case ${e.message} is not yet implemented");
          break;
      }
      throw Exception(authError);
    }
  }

  Future<User> loginUser(String email, String password) async {
    try {
      UserCredential result = await firebaseAuth.signInWithEmailAndPassword(
          email: email,
          password: password,
      );
      return result.user;
    }
    catch (e) {
      String authError = "";
      print("exception >>>>>> ${e.code}");
      print("exception msg >>>>>> ${e.message}");
      switch(e.code){
        case 'user-not-found': authError = "User does not exist";
        break;
        case 'wrong-password': authError = "The password is incorrect. Try again.";
        break;
        case 'network-request-failed': authError = "A network error has occurred";
        break;
        default:
          print("Case ${e.message} is not yet implemented");
          break;
      }
      throw Exception(authError);
    }

  }
    //   throw Exception(e.toString());
    // }


  Future<void> resetPassword(String email) async {
    try {
      var result = await firebaseAuth.sendPasswordResetEmail(
        email: email,
      );
      return result;
    }
    catch (e) {
      String resetError = "";
      print("exception msg >>>>>> ${e.message}");
      print("exception code >>>>>> ${e.code}");
      switch(e.code){
        case 'user-not-found': resetError = "This email does not exist";
        break;
        case 'network-request-failed': resetError = "A network error has occurred";
        break;
        default:
          print("Case ${e.message} is not yet implemented");
      }

      throw Exception(resetError);
    }
  }

  Future<void> logOut() async{
    await firebaseAuth.signOut();
  }

  // Future<void> resetPassword(String email) async{
  //   await firebaseAuth.sendPasswordResetEmail(email: email);
  // }

  Future<bool> isLoggedIn() async{
    var currentUser = await firebaseAuth.currentUser;
    return currentUser != null;
  }

  Future<User> getCurrentUser() async{
    return await firebaseAuth.currentUser;
  }

  Future<User> signInWithGoogle() async {
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

      AuthCredential credential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken
      );

      await firebaseAuth.signInWithCredential(credential);
      return firebaseAuth.currentUser;

      // User user = firebaseAuth.currentUser;
      // print(user.uid);
      //
      // return Future.value(true);
    }
  }


  Future<User> loginWithFacebook() async {
    FacebookLogin facebookLogin = FacebookLogin();

    final result = await facebookLogin.logIn(['email']);
    FacebookAccessToken token = result.accessToken;

    debugPrint(result.status.toString());

    if (result.status == FacebookLoginStatus.loggedIn){
      print("User is Loggedd in");

      final credential = FacebookAuthProvider.credential(token.token);
      await firebaseAuth.signInWithCredential(credential);
      return await firebaseAuth.currentUser;
    }
    else if (result.status == FacebookLoginStatus.cancelledByUser){
      print("User is cancelledd");
      return null;

    }
  }

}